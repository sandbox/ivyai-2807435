**************************************
The Ivy module for Drupal
by Ivy team
**************************************

The Ivy module provides some simple integration between the Ivy live chat
service and Drupal.

In addition to adding the Ivy code to your page, the module also tells the
operator whether the user is logged in and if so, what their username, email
address, and user page are.

Installation
============
1. Create and configure an account at [Ivy](http://www.ivy.ai).
2. Place this module in your modules directory and enable it.
3. Visit http://www.ivy.ai/install and copy the code chunk
4. Visit admin/settings/services/ivy on your Drupal site and paste the code
   chunk into the textarea. Submit the settings form.

Ivy Advanced Settings
=======================
To enable Ivy on Android phones please ensure the "Enable on Smart Phones"
option is checked in Ivy advanced settings. The advanced settings can be
accessed at: https://www.ivy.ai/customize/config?customize_form_pane=advanced
