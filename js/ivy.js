/*jslint browser: true*/
/*global jQuery, Drupal */

/**
 * @file
 * Attaches behaviors for Ivy iOS and users.
 */

(function (Drupal, ivy, navigator) {

  'use strict';

  /**
   * Ivy behavior.
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.ivy = {
    attach: function (context, settings) {
      if (ivy === undefined) {
        return;
      }

      // Providing user details to the Ivy API.
      if (settings.ivy.uid !== undefined) {
        ivy('api.visitor.getDetails', function (details) {
          ivy('api.chat.updateVisitorNickname', {
            snippet: settings.ivy.name,
            hidesDefault: true
          });

          ivy('api.chat.updateVisitorStatus', {
            snippet: settings.ivy.mail + ' | ' + settings.ivy.userpage
          });

          ivy('api.visitor.updateFullName', { fullName: settings.ivy.name });
          ivy('api.visitor.updateEmailAddress', { emailAddress: settings.ivy.mail });
        });
      }

      // Hides Ivy box if agent is iPod, iPad, iPhone.
      if (settings.ivy.disable_ios && settings.ivy.enabled) {
        ivy('api.box.onShow', function () {
          var agent = navigator.userAgent.toLowerCase();
          if (agent.match(/iP(hone|ad)/i)) {
            ivy('api.box.hide');
          }
        });
      }
    }
  };
}(Drupal, ivy, navigator));
