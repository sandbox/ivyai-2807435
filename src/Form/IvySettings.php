<?php

/**
 * @file
 * Contains \Drupal\ivy\Form\IvySettings.
 */

namespace Drupal\ivy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class IvySettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ivy_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ivy.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ivy.settings'];
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $settings = \Drupal::config('ivy.settings');
    $form['ivy_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ivy code'),
      '#description' => $this->t('Paste the Javascript code block from <a href="http://ivy.ai/install">Ivy.ai</a>'),
      '#default_value' => $settings->get('ivy_code'),
      '#attributes' => ['placeholder' => '<!-- begin ivy code -->'],
    ];
    $form['ivy_ios'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable in iOS devices'),
      '#description' => $this->t('Hides it on iPhone, iPad and iPod since it has issues in this platforms.'),
      '#default_value' => $settings->get('ivy_ios'),
    ];
    $form['ivy_enable_admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable on admin pages.'),
      '#description' => $this->t('Embeds the ivy code on admin pages.'),
      '#default_value' => $settings->get('ivy_enable_admin'),
    ];

    return parent::buildForm($form, $form_state);
  }

}
